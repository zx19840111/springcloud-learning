package com.zx.ms.test.springcloud.feignconsumer.api;

import com.zx.ms.test.springcloud.feignconsumer.bean.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(name="HELLO-DEV",fallback = FeignConsumerInterfaceFallback.class)
//关闭Hystrix 方式
//@FeignClient(name="HELLO-DEV" configuration = DisableHystrixConfiguration.class)
public interface FeignConsumerInterface {

    @GetMapping("/hello")
    public String hello();

    @RequestMapping(value = "hello2",method = RequestMethod.GET)
    String hello(@RequestParam("name") String name);

    @RequestMapping(value = "hello2",method = RequestMethod.GET)
    String hello(@RequestParam("name") String name,@RequestParam("age") Integer age);

    @RequestMapping(value = "hello3",method = RequestMethod.GET)
    String hello(@RequestBody User user);

}
