package com.zx.ms.test.springcloud.feignconsumer.api;

import com.zx.ms.test.springcloud.feignconsumer.bean.User;
import org.springframework.stereotype.Component;

@Component
public class FeignConsumerInterfaceFallback implements FeignConsumerInterface {
    @Override
    public String hello() {
        return "o no!!";
    }

    @Override
    public String hello(String name) {
        return "o no!!";
    }

    @Override
    public String hello(String name, Integer age) {
        return "o no!!";
    }

    @Override
    public String hello(User user) {
        return "o no!!";
    }
}
