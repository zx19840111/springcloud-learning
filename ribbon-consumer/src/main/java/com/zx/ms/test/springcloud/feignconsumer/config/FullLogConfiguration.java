package com.zx.ms.test.springcloud.feignconsumer.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FullLogConfiguration {

    @Bean
    Logger.Level feignLoggerLevel(){
        //四种级别
        //Logger.Level.BASIC; 仅记录请求方法，URL，相应状态码，执行时间
        //Logger.Level.HEADERS;  basic + 请求和响应头信息
        //Logger.Level.NONE;  不记录任何
        //Logger.Level.FULL: 所有信息

        return Logger.Level.FULL;
    }
}
