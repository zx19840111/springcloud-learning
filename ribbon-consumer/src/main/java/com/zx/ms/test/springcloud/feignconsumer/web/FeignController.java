package com.zx.ms.test.springcloud.feignconsumer.web;

import com.zx.ms.test.springcloud.feignconsumer.api.FeignConsumerInterface;
import com.zx.ms.test.springcloud.feignconsumer.bean.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class FeignController {

    @Autowired
    private FeignConsumerInterface feignConsumerInterface;

    @RequestMapping(value="/feign-consumer-1" ,method = RequestMethod.GET)
    public String helloConsumer1(){
        return feignConsumerInterface.hello();
    }

    @RequestMapping(value="/feign-consumer-2" ,method = RequestMethod.GET)
    public String helloConsumer2(){
        return feignConsumerInterface.hello("赵新");
    }

    @RequestMapping(value="/feign-consumer-3" ,method = RequestMethod.GET)
    public String helloConsumer3(){
        return feignConsumerInterface.hello("赵新",2000);
    }

    @RequestMapping(value="/feign-consumer-4" ,method = RequestMethod.GET)
    public String helloConsumer4(){
        return feignConsumerInterface.hello(new User("赵新1",199999));
    }

}
