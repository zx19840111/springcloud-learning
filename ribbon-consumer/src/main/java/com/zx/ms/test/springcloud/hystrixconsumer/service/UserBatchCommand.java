package com.zx.ms.test.springcloud.hystrixconsumer.service;

import com.netflix.hystrix.HystrixCommand;

import java.util.ArrayList;
import java.util.List;

import static com.netflix.hystrix.HystrixCommandGroupKey.Factory.asKey;

public class UserBatchCommand extends HystrixCommand<List<String>> {

    private UserService userService;
    private List<String> userIds;

    public UserBatchCommand(UserService userService,List<String> userIds){
        super(Setter.withGroupKey(asKey("userServiceCommand")));
        this.userIds = userIds;
        this.userService = userService;
    }

    @Override
    protected List<String> run() throws Exception {
        return userService.findAll(userIds);
    }



    /**
     * 自定义降级异常处理
     * @return
     */
    @Override
    protected List<String > getFallback() {
        List<String> result = new ArrayList<>();
        userIds.forEach(k-> result.add("u a error"));

        return result;

    }
}
