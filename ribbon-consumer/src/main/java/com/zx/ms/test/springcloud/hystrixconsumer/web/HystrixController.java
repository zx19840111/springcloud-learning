package com.zx.ms.test.springcloud.hystrixconsumer.web;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.strategy.concurrency.HystrixRequestContext;
import com.zx.ms.test.springcloud.hystrixconsumer.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import rx.Observable;
import rx.Observer;
import rx.functions.Action1;

import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@RestController
public class HystrixController {

    @Autowired
    HelloService helloService;

    @Autowired
    UserService userService;

    @Autowired
    RestTemplate restTemplate;

    @RequestMapping(value = "hystrixHelloConsumer",method = RequestMethod.GET)
    public String hystrixHelloConsumer(){
        return helloService.helloService();
    }

    @RequestMapping(value = "hystrixUserService",method = RequestMethod.GET)
    public String hystrixUserService(){
        return userService.getUserById(1L);
    }

    @RequestMapping(value = "hystrixUserService1",method = RequestMethod.GET)
    public String hystrixUserService1() throws ExecutionException, InterruptedException {
        return userService.getUserByIdAsync(1L).get();
    }

    @RequestMapping(value = "hystrixUserService2",method = RequestMethod.GET)
    public String hystrixUserService2(){
        Observable<String> os = userService.getUserByIdAsync2("1");
        //注册完整执行生命周期事件
        os.subscribe(new Observer<String>() {
            @Override
            public void onCompleted() {
                // onNext/onError完成之后最后回调
                System.out.println("execute onCompleted");
            }
            @Override
            public void onError(Throwable e) {
                // 当产生异常时回调
                System.out.println("onError " + e.getMessage());
                e.printStackTrace();
            }
            @Override
            public void onNext(String v) {
                // 获取结果后回调
                System.out.println("onNext: " + v);
            }
        });

        return "异步执行";
    }


    @RequestMapping(value = "hystrixUserCommand",method = RequestMethod.GET)
    public String hystrixUserCommand(){
        return new UserCommand(restTemplate,1L).execute();
    }

    @RequestMapping(value = "hystrixUserCommand1",method = RequestMethod.GET)
    public String hystrixUserCommand1() throws ExecutionException, InterruptedException {
        //异步
        Future<String> futureUser = new UserCommand(restTemplate,1L).queue();
        return futureUser.get();
    }

    @RequestMapping(value = "hystrixUserCommand2",method = RequestMethod.GET)
    public String hystrixUserCommand2() throws ExecutionException, InterruptedException {

        //Rxjava 的观察者模式
        //HOT  observable
        Observable<String> os = new UserCommand(restTemplate,1L).observe();
        //简要模式
//        os.subscribe(new Action1<String>() {
//            @Override
//            public void call(String s) {
//                //执行结果
//                System.out.println(s);
//            }
//        });

        //注册完整执行生命周期事件
        os.subscribe(new Observer<String>() {
            @Override
            public void onCompleted() {
                // onNext/onError完成之后最后回调
                System.out.println("execute onCompleted");
            }
            @Override
            public void onError(Throwable e) {
                // 当产生异常时回调
                System.out.println("onError " + e.getMessage());
                e.printStackTrace();
            }
            @Override
            public void onNext(String v) {
                // 获取结果后回调
                System.out.println("onNext: " + v);
            }
        });

        return "异步执行";

    }

    @RequestMapping(value = "hystrixUserCommand3",method = RequestMethod.GET)
    public String hystrixUserCommand3() throws ExecutionException, InterruptedException {

        //Rxjava 的观察者模式
        //cold observable
        Observable<String> os = new UserCommand(restTemplate,1L).toObservable();

        //subscribe 只能执行一次 否则出现 CommandKeyName command executed multiple times异常
        //该模型源码 校验命令的状态，保证其只执行一次

//        os.subscribe(new Action1<String>() {
//            @Override
//            public void call(String s) {
//                //执行结果
//                System.out.println(s);
//            }
//        });

        //注册完整执行生命周期事件
        os.subscribe(new Observer<String>() {
            @Override
            public void onCompleted() {
                // onNext/onError完成之后最后回调
                System.out.println("execute onCompleted");
            }
            @Override
            public void onError(Throwable e) {
                // 当产生异常时回调
                System.out.println("onError " + e.getMessage());
                e.printStackTrace();
            }
            @Override
            public void onNext(String v) {
                // 获取结果后回调
                System.out.println("onNext: " + v);
            }
        });

        return "异步执行";

    }

    @RequestMapping(value = "hystrixUserObservableCommand",method = RequestMethod.GET)
    public String hystrixUserObservableCommand() throws ExecutionException, InterruptedException {

        //Rxjava 的观察者模式
        //cold observable
        Observable<String> os = new UserObservableCommand(restTemplate,1L).observe();

        //subscribe 只能执行一次 否则出现 CommandKeyName command executed multiple times异常
        //该模型源码 校验命令的状态，保证其只执行一次

//        os.subscribe(new Action1<String>() {
//            @Override
//            public void call(String s) {
//                //执行结果
//                System.out.println(s);
//            }
//        });

        //注册完整执行生命周期事件
        os.subscribe(new Observer<String>() {
            @Override
            public void onCompleted() {
                // onNext/onError完成之后最后回调
                System.out.println("execute onCompleted");
            }
            @Override
            public void onError(Throwable e) {
                // 当产生异常时回调
                System.out.println("onError " + e.getMessage());
                e.printStackTrace();
            }
            @Override
            public void onNext(String v) {
                // 获取结果后回调
                System.out.println("onNext: " + v);
            }
        });

        return "异步执行";

    }

    @RequestMapping(value = "hystrixUserObservableCommand1",method = RequestMethod.GET)
    public String hystrixUserObservableCommand1() throws ExecutionException, InterruptedException {

        //Rxjava 的观察者模式
        //cold observable
        Observable<String> os = new UserObservableCommand(restTemplate,1L).toObservable();

        //subscribe 只能执行一次 否则出现 CommandKeyName command executed multiple times异常
        //该模型源码 校验命令的状态，保证其只执行一次

//        os.subscribe(new Action1<String>() {
//            @Override
//            public void call(String s) {
//                //执行结果
//                System.out.println(s);
//            }
//        });

        //注册完整执行生命周期事件
        os.subscribe(new Observer<String>() {
            @Override
            public void onCompleted() {
                // onNext/onError完成之后最后回调
                System.out.println("execute onCompleted");
            }
            @Override
            public void onError(Throwable e) {
                // 当产生异常时回调
                System.out.println("onError " + e.getMessage());
                e.printStackTrace();
            }
            @Override
            public void onNext(String v) {
                // 获取结果后回调
                System.out.println("onNext: " + v);
            }
        });

        return "异步执行";

    }

    @RequestMapping(value = "hystrixUserCollapseCommand",method = RequestMethod.GET)
    public String hystrixUserCollapseCommand() throws ExecutionException, InterruptedException {

        Future<String> future1 = new UserCollapseCommand(userService,"1").queue();
        Future<String> future2 = new UserCollapseCommand(userService,"2").queue();
        System.out.println(future1.get());
        System.out.println(future2.get());

        Thread.currentThread().sleep(300);

        Future<String> future3 = new UserCollapseCommand(userService,"3").queue();
        System.out.println(future3.get());
        return "操作成功";
    }

    @RequestMapping(value = "hystrixUserCollapseCommand1",method = RequestMethod.GET)
    public String hystrixUserCollapseCommand1() throws ExecutionException, InterruptedException {

        System.out.println(userService.find("1"));
        System.out.println(userService.find("2"));

        Thread.currentThread().sleep(300);

        System.out.println(userService.find("3"));

        return "操作成功";
    }


}
