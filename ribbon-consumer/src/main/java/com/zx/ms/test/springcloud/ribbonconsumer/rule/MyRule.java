package com.zx.ms.test.springcloud.ribbonconsumer.rule;

import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.*;

import java.util.List;

public class MyRule extends AbstractLoadBalancerRule {

    private ILoadBalancer lb;

    @Override
    public Server choose(Object key) {
        System.out.println(key+"11111111111111111111111111111111");
        List<Server> servers = lb.getAllServers();
        for (Server server : servers) {
            System.out.println(server.getHostPort());

        }
        return servers.get(0);
    }

    @Override
    public void setLoadBalancer(ILoadBalancer iLoadBalancer) {
        this.lb = lb;
    }


    @Override
    public ILoadBalancer getLoadBalancer() {
        return lb;
    }

    @Override
    public void initWithNiwsConfig(IClientConfig iClientConfig) {

    }
}
