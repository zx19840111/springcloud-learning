package com.zx.ms.test.springcloud.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
//Eureka 2020版本
//@EnableEurekaClient
//Eureka1。x
@EnableDiscoveryClient
@SpringBootApplication
public class DemoApplication {
    //启动命令 java -jar xxx.jar --spring.profiles.active=prod 可以通过命令行传入参数
    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

}
