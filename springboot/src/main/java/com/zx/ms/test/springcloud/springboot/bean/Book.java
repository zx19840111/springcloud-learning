package com.zx.ms.test.springcloud.springboot.bean;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class Book {

    //获取配置文件中内容
    @Value("${book.name}")
    private String name;
    //获取配置文件中内容
    @Value("${book.author}")
    private String author;
    //获取配置文件中的内容
    @Value("${book.desc}")
    private String desc;
    @Value("${com.zx.ms.test.springcloud.springboot.random.value}")
    private String value;
    @Value("${com.zx.ms.test.springcloud.springboot.random.number}")
    private Integer number;
    @Value("${com.zx.ms.test.springcloud.springboot.random.bignumber}")
    private Long bigNumber;
    @Value("${com.zx.ms.test.springcloud.springboot.random.test1}")
    private Integer test1;
    @Value("${com.zx.ms.test.springcloud.springboot.random.test2}")
    private Integer test2;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Long getBigNumber() {
        return bigNumber;
    }

    public void setBigNumber(Long bigNumber) {
        this.bigNumber = bigNumber;
    }

    public Integer getTest1() {
        return test1;
    }

    public void setTest1(Integer test1) {
        this.test1 = test1;
    }

    public Integer getTest2() {
        return test2;
    }

    public void setTest2(Integer test2) {
        this.test2 = test2;
    }

    public String toString(){
        return name + ":" + author + ":" + desc + ":" + value + ":"
                + number + ":" + bigNumber + ":" + test1 + ":" + test2;
    }
}
