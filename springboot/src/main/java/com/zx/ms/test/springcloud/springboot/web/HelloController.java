package com.zx.ms.test.springcloud.springboot.web;


import com.zx.ms.test.springcloud.springboot.bean.Book;
import com.zx.ms.test.springcloud.springboot.bean.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


@RestController
public class HelloController {

    private final Logger logger = Logger.getLogger(getClass().getName());

    @Autowired
    private Book book;

    @Autowired
    private DiscoveryClient discoveryClient;

    @RequestMapping("/hello")
    public String hello(){
        List<String> list = discoveryClient.getServices();

        System.out.println("******" + list);

        List<ServiceInstance>  serviceInstances = discoveryClient.getInstances("hello-dev");

        for(ServiceInstance serviceInstance:serviceInstances) {

            System.out.println(serviceInstance.getServiceId() + "\t" +

                    serviceInstance.getHost() + "\t" +

                    serviceInstance.getPort() + "\t" +

                    serviceInstance.getUri());

        }
        logger.info(book.toString());
       // logger.info("/hello,host:" + instance.getHost() + ",service_id:" + instance.getServiceId());
        return "hello world";
    }






    @RequestMapping("/helloAll")
    public List<String> helloAll(){
        List<String> list = new ArrayList<>();
        list.add("你");
        list.add("好");
        list.add("么");
        return list;
    }

    @RequestMapping(value = "hello2" , method = RequestMethod.GET)
    public User hello2(@RequestParam String name , @RequestParam Integer age){
        return new User(name,age);
    }

    @RequestMapping(value = "hello3" , method = RequestMethod.POST)
    public String hello3(@RequestBody User user) {
        return "Hello " + user.getName() + user.getAge();
    }

}
